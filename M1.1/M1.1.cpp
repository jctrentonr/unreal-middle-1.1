
#include <iostream>
#include <cmath>

class Vector
{
public:
	Vector() {
		x = 0;
		y = 0;
		z = 0;
	}

	Vector(float x, float y, float z) {
		this->x = x;
		this->y = y;
		this->z = z;
		Info = new std::string("important!");
	}

	// explicit ������� ����������� ������������� ������������ ��������������, �.�. ������ ����� ������������ ����������� ���� Vector v = 1;
	explicit Vector(float num) {
		x = num;
		y = num;
		z = num;
	}

	Vector(const Vector& other) {
		std::cout << "\nCopy constructor \n";
		x = other.x;
		y = other.y;
		z = other.z;

		Info = new std::string(*(other.Info));
	}

	~Vector() {
		std::cout << "\nDesctructor calling\n";
		delete Info;
	}

	operator float() {
		return sqrt(x * x + y * y + z * z);
	}

	friend Vector operator*(const Vector& a, float mult);

	friend Vector operator-(const Vector& a, const Vector& b);

	friend Vector operator+(const Vector& a, const Vector& b);

	friend std::ostream& operator<<(std::ostream& out, const Vector& v);

	friend std::istream& operator>>(std::istream& in, Vector& v);

	friend bool operator>(const Vector& a, const Vector& b);

	float operator[](int index) {
		switch (index)
		{
		case 0:
			return x;
			break;
		case 1:
			return y;
			break;
		case 2:
			return z;
			break;
		default:
			std::cout << "Index error";
			return 0;
			break;
		}
	}

private:
	float x;
	float y;
	float z;

	std::string* Info;
};

Vector operator*(const Vector& a, float mult) {
	return Vector(
		a.x * mult,
		a.y * mult,
		a.z * mult
	);
};

Vector operator-(const Vector& a, const Vector& b) {
	return Vector(
		a.x - b.x,
		a.y - b.y,
		a.z - b.z
	);
}

Vector operator+(const Vector& a, const Vector& b) {

	return Vector(
		a.x + b.x,
		a.y + b.y,
		a.z + b.z
	);
}

std::ostream& operator<<(std::ostream& out, const Vector& v) {
	out << v.x << " " << v.y << " " << v.z;
	return out;
}

std::istream& operator>>(std::istream& in, Vector& v) {
	in >> v.x;
	in >> v.y;
	in >> v.z;
	return in;
}

bool operator>(const Vector& a, const Vector& b) {
	return false;
}

class A {

	A(int a) {
		test = a;
	}

	// ��������� ������������� ��������� ������������
	A(char a) = delete;

private:
	int test;
};

int main()
{

	Vector v0(1);
	Vector v1(0, 1, 2);
	Vector v2(3, 4, 5);
	Vector v3 = v1 + v2;

	std::cout << v3 << "\n";
	std::cout << v1[2] << "\n";
	std::cout << "Vector length: " << float(v3) << "\n";

	std::cout << v1 * 3.f << "\n";
	std::cout << v1 - v2 << "\n";

	std::cout << "Enter x, y and z of new Vector:\n";
	std::cin >> v2;
	std::cout << "\n" << v2;
}
